import React from "react";

function Scroll() {
  return (
    <div className="scroll" onClick={top}>
      <i className="fa fa-arrow-up"></i>
    </div>
  );
}
function top() {
  window.scrollTo(0, 0);
}
export default Scroll;
