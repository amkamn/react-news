import styled from "styled-components";

export const MainContainer = styled.div`
  height: auto;
  width: 100%;
  box-sizing: border-box;
  padding: 15px;
  background: #34495e;
`;

export const MainRow = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  width: 90%;
  margin: auto;
  margin-top: 2%;
  margin-bottom: 3%;
  @media (max-width: 768px) {
    width: 100%;
  }
  @media (max-width: 1200px) and (min-width: 769px) {
    width: 100%;
  }
`;

export const FooterDiv = styled.div`
  width: 25%;
  @media (max-width: 768px) {
    width: 100%;
  }
  @media (max-width: 1200px) and (min-width: 769px) {
    width: 50%;
  }
`;
export const FooterInner = styled.div`
  padding: 10px;
`;
export const TitleFooter = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: #fff;
`;

export const TextFooter = styled.div`
  font-size: 13px;
  margin-top: 30px;
  color: #fff;
`;
