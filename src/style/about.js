import styled from "styled-components";

export const MyWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row wrap;
  height: 60vh;
`;

export const MyInfo = styled.div`
  width: 100%;
`;

export const Social = styled.div`
  margin-top: 10%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  i {
    margin-left: 15px;
    font-size: 20px;
    cursor: pointer;
  }
  a {
    color: inherit;
    transition: all 0.3s ease-in-out;
    &:hover {
      color: blue;
    }
  }
`;
