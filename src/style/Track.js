import styled from "styled-components";

export const MainContainer = styled.div`
  height: auto;
  width: 100%;
  box-sizing: border-box;
`;

export const MainRow = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  width: 100%;
  margin: auto;
  margin-top: 3%;
  margin-bottom: 3%;
`;

export const TrackTitle = styled.p`
  font-size: 20px;
  font-weight: bold;
  width: 100%;
  marign-left: 15px;
`;
export const TrackDiv = styled.div`
  width: 25%;
  margin-top: 10px;
  @media (max-width: 768px) {
    width: 100%;
    margin-top: 10px;
    img {
      width: 100% !important;
    }
  }
  @media (max-width: 1200px) and (min-width: 769px) {
    width: 50%;
  }
  img {
    width: 90%;
    margin: auto;
    display: block;
    cursor: pointer;
  }
`;
