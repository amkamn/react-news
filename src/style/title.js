import styled from "styled-components";
export const Title = styled.p`
  font-size: 3em;
  background-image: linear-gradient(to right top, #d16ba5, #c777b9, #ba83ca, #aa8fd8, #9a9ae1, #8aa7ec, #79b3f4, #69bff8, #52cffe, #41dfff, #46eefa, #5ffbf1);
  text-shadow: 1px 8px 14px rgba(150, 150, 150, 1);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  margin:0;
  font-weight:bold;
  transition:all 0.5s ease;
  cursor:pointer;
  &:hover {
    text-shadow: 1px 14px 13px rgba(150, 150, 150, 1);
    background-image: linear-gradient(to right top, #d24d98, #c75fb1, #b870c7, #a67fd8, #918ee4, #799df0, #5faaf8, #41b7fc, #00c8ff, #00d7fc, #00e5f3, #3cf2e6);
  }
`;
