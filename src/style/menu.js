import styled from "styled-components";
import { Link } from "react-router-dom";

export const MainContainer = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  background: #34495e;
  height: 70px;
`;

export const CustomLink = styled(Link)`
  color: #fff;
  padding-bottom: 20px;
  text-decoration: none;
  padding-right: 20px;
  &: {
    text-decration: underline;
  }
`;
