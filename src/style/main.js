import styled from "styled-components";

export const MainContainer = styled.div`
  height: auto;
  width: 100%;
  box-sizing: border-box;
  padding-left: 15px;
  padding-right: 15px;
`;
