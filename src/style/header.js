import styled from "styled-components";

export const HeaderContainer = styled.div`
    height: auto;
    width:100%;
    padding-top:2em;
`;

export const SearchContainer = styled.div`
    height: auto;
    width:100%;
    padding-top:2em;
`;

export const InputContainer = styled.input`
    height: 30px;
    outline:none;
    width:97.4%;
    border-image-source: linear-gradient(to right top, #d24d98, #c75fb1, #b870c7, #a67fd8, #918ee4, #799df0, #5faaf8, #41b7fc, #00c8ff, #00d7fc, #00e5f3, #3cf2e6);
    border-width: 2.5pt;
    border-image-slice: 1;
    font-weight:bold;
    padding-left:1%;
    padding-right:1%;
    text-transform:uppercase;
`;


export const SearchButton = styled.div`
    margin-top:2em;
    width:100px;
    padding:10px;
    text-transform:uppercase;
    color:#fff;
    font-weight:bold;
    display:flex;
    justify-content:center;
    cursor:pointer;
    border-radius:4px;
    transition:all 0.3s ease-in-out;
    box-shadow: 3px 5px 3px rgba(150, 150, 150, 0.6);
    background-image: linear-gradient(to right top, #d16ba5, #c777b9, #ba83ca, #aa8fd8, #9a9ae1, #8aa7ec, #79b3f4, #69bff8, #52cffe, #41dfff, #46eefa, #5ffbf1);
    &:hover{
        box-shadow: 6px 6px 6px rgba(150, 150, 150, 1);
    }
`;