import styled from "styled-components";

export const ContentContainer = styled.div`
  min-height: auto;
  width: 100%;
`;

export const CardWrapper = styled.div`
  width: 100%;
  margin-top: 1em;
  margin-bottom: 1em;
  display: flex;
  flex-direction: wrap;
  flex-wrap: wrap;
`;

export const CardList = styled.div`
  width: 25%;
  height: auto;
  box-sizing: border-box;
  margin-top: 1em;
  @media (max-width: 768px) {
    width: 100%;
  }
  @media (max-width: 1200px) and (min-width: 769px) {
    width: 50%;
  }
`;

export const CardListItem = styled.div`
  border: 3px solid;
  border-image-source: linear-gradient(
    to right top,
    #d24d98,
    #c75fb1,
    #b870c7,
    #a67fd8,
    #918ee4,
    #799df0,
    #5faaf8,
    #41b7fc,
    #00c8ff,
    #00d7fc,
    #00e5f3,
    #3cf2e6
  );
  border-image-slice: 1;
  width: 93%;
  margin: auto;
  height: auto;
  @media (max-width: 768px) {
    width: 100%;
  }
  @media (max-width: 1200px) and (min-width: 769px) {
    width: 95%;
  }
`;
export const CardImage = styled.img`
  width: 100%;
  height: 250px;
  object-fit: cover;
`;

export const CardText = styled.a`
  font-size: 13px;
  display: block;
  margin: 0;
  font-weight: bold;
  padding: 10px 10px 20px 10px;
  height: 35px;
  overflow: hidden;
  cursor: pointer;
  background-image: linear-gradient(
    to right top,
    #d16ba5,
    #c777b9,
    #ba83ca,
    #aa8fd8,
    #9a9ae1,
    #8aa7ec,
    #79b3f4,
    #69bff8,
    #52cffe,
    #41dfff,
    #46eefa,
    #5ffbf1
  );
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;
