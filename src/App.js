import React, { useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Navigation from "./router/index";
import Menu from "./views/Menu";
import Footer from "./views/Footer";
import Scroll from "./helper/scrollTop";
import { useDispatch, useSelector } from "react-redux";
import allActions from "./actions/index";

const App = () => {
  const scroll = useSelector(state => state.scrollToTop.scroll);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(allActions.ScrollToTop.CalcScroll());
  }, []);

  return (
    <Router>
      <Menu />
      <Navigation />
      <Footer />
      {scroll === true && <Scroll />}
    </Router>
  );
};

export default App;
