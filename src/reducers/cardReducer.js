import { FETCH_POSTS, FETCH_ERROR } from "../actions/types";

const initialState = {
  items: [],
  loading: "loading"
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        items: action.payload.items,
        loading: action.payload.loading
      };
    case FETCH_ERROR:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
}
