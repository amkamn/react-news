import { SET_LOGIN, SET_LOGOUT } from "../actions/types";

const initialState = {
  isloggedin: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_LOGIN:
      return {
        ...state,
        isloggedin: action.payload
      };
    case SET_LOGOUT:
      return {
        ...state,
        isloggedin: action.payload
      };
    default:
      return state;
  }
}
