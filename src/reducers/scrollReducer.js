import { SET_SCROLL } from "../actions/types";

const initialState = {
  scroll: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_SCROLL:
      return {
        ...state,
        scroll: action.payload
      };
    default:
      return state;
  }
}
