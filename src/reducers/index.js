import { combineReducers } from "redux";
import cardReducer from "./cardReducer";
import searchReducer from "./searchReducer";
import scrollToTop from "./scrollReducer";
import User from "./userReducer";

export default combineReducers({
  item: cardReducer,
  search: searchReducer,
  scrollToTop: scrollToTop,
  user: User
});
