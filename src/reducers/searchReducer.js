import { FETCH_SEARCH_VALUE,SET_SEARCH_VALUE } from '../actions/types';

const initialState = {
  items: [],
  value:'',
  loading:'loading',
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_SEARCH_VALUE:
        return {
        ...state,
        items: action.payload.items,
        loading:action.payload.loading
    };
    case SET_SEARCH_VALUE:
        return {
            ...state,
            value:action.payload
        }
    default:
      return state;
  }
}