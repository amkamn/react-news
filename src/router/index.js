import React, { memo } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import About from "../views/About";
import Home from "../views/Home";
import Signin from "../views/signin";
import Protected from "./Protected";
import { useSelector } from "react-redux";

export default memo(function index() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/about" component={About} />
      <Route path="/Menu2" component={Menu2} />
      <Route path="/Menu3" component={Menu3} />
      <Route path="/Menu4" component={Menu4} />
      <Route path="/Signin" component={Signin} />
      <PrivateRoute path="/dashboard" component={Protected} />
      <Route component={Home} />
    </Switch>
  );
});

const PrivateRoute = ({ component: Component, ...rest }) => {
  let islogged = useSelector(state => state.user.isloggedin);
  return (
    <Route
      {...rest}
      render={props =>
        islogged === true ? <Component {...props} /> : <Redirect to="/signin" />
      }
    />
  );
};

function Menu2() {
  return <div className="customMenu">Menu2</div>;
}
function Menu3() {
  return <div className="customMenu">Menu3</div>;
}
function Menu4() {
  return <div className="customMenu">Menu4</div>;
}
