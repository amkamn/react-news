import React from "react";
import {
  MainContainer,
  MainRow,
  FooterDiv,
  TitleFooter,
  TextFooter,
  FooterInner
} from "../style/footer";

function Footer() {
  return (
    <MainContainer>
      <MainRow>
        <FooterDiv>
          <FooterInner>
            <TitleFooter>Menu 1</TitleFooter>
            <TextFooter>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged.
            </TextFooter>
          </FooterInner>
        </FooterDiv>
        <FooterDiv>
          <FooterInner>
            <TitleFooter>Menu 1</TitleFooter>
            <TextFooter>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged.
            </TextFooter>
          </FooterInner>
        </FooterDiv>
        <FooterDiv>
          <FooterInner>
            <TitleFooter>Menu 1</TitleFooter>
            <TextFooter>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged.
            </TextFooter>
          </FooterInner>
        </FooterDiv>
        <FooterDiv>
          <FooterInner>
            <TitleFooter>Menu 1</TitleFooter>
            <TextFooter>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged.
            </TextFooter>
          </FooterInner>
        </FooterDiv>
      </MainRow>
    </MainContainer>
  );
}

export default Footer;
