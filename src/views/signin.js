import React from "react";
import { useDispatch } from "react-redux";
import allActions from "../actions/index";
import { useHistory } from "react-router-dom";

function Signin() {
  const dispatch = useDispatch();
  const history = useHistory();
  function changeUrl() {
    dispatch(allActions.User.login());
    history.push("/dashboard");
  }
  return (
    <div className="customMenu">
      <button onClick={() => changeUrl()}>Login</button>
    </div>
  );
}

export default Signin;
