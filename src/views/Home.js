import React, { memo, useEffect } from "react";
import { MainContainer } from "../style/main";
import Header from "./Header";
import Content from "./Content";
import Track from "./Track";

export default memo(function Home() {
  useEffect(() => {
    document.title = "Home Page";
  }, []);
  return (
    <MainContainer>
      <Header />
      <Content />
      <Track />
    </MainContainer>
  );
});
