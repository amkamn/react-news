import React from "react";
import { MainContainer, MainRow, TrackDiv, TrackTitle } from "../style/Track";

function Track() {
  return (
    <MainContainer>
      <MainRow>
        <TrackTitle>Tracking</TrackTitle>
        <TrackDiv>
          <a href="http://google.com" target="_blank">
            <img src="https://reactjs.org/logo-og.png" />
          </a>
        </TrackDiv>
        <TrackDiv>
          <a href="http://facebook.com" target="_blank">
            <img src="https://reactjs.org/logo-og.png" />
          </a>
        </TrackDiv>
        <TrackDiv>
          <a href="http://youtube.com" target="_blank">
            <img src="https://reactjs.org/logo-og.png" />
          </a>
        </TrackDiv>
        <TrackDiv>
          <a href="http://dev.to" target="_blank">
            <img src="https://reactjs.org/logo-og.png" />
          </a>
        </TrackDiv>
      </MainRow>
    </MainContainer>
  );
}

export default Track;
