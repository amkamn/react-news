import React from 'react';
import {HeaderContainer } from '../style/header';
import Title from '../components/SiteTitle';
import Search from '../components/Search';

function Header() {
  return (
    <HeaderContainer>
        <Title />
        <Search />
    </HeaderContainer>
  );
}
export default Header;
