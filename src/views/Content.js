import React, { useEffect } from "react";
import { CardWrapper } from "../style/content";
import { ContentContainer } from "../style/content";
import NoData from "../components/NoData";
import Error from "../components/Error";
import Loader from "../components/Loader";
import Card from "../components/Card";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../actions/index";

function Content() {
  const item = useSelector(state => state.item.items);
  const loading = useSelector(state => state.item.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(allActions.ListActions.FetchData());
  }, []);

  const renderCard = () => {
    return Object.keys(item).map((obj, index) => {
      return <Card data={item[obj]} key={index} />;
    });
  };
  if (loading === "ok") {
    return (
      <ContentContainer>
        <CardWrapper>{renderCard()}</CardWrapper>
      </ContentContainer>
    );
  } else if (loading === "no-data") {
    return <NoData />;
  } else if (loading === "loading") {
    return <Loader />;
  } else {
    return <Error />;
  }
}
export default Content;
