import React, { memo, useState, useEffect } from "react";
import { MainContainer, CustomLink } from "../style/menu";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../actions/index";
import { useHistory } from "react-router-dom";

export default memo(function Menu() {
  const [width, setWidth] = useState(0);
  const [show, setShow] = useState(false);
  let islogged = useSelector(state => state.user.isloggedin);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    let width = window.innerWidth;
    setWidth(width);
    CalcWidth();
  }, []);

  function CalcWidth() {
    window.addEventListener("resize", () => {
      let width = window.innerWidth;
      setWidth(width);
    });
  }
  function logout() {
    dispatch(allActions.User.logout());
    history.push("/");
  }

  function isUser() {
    if (islogged === true) {
      return (
        <>
          <CustomLink to="/dashboard">Dashboard</CustomLink>
          <a
            href="#"
            style={{ color: "#fff", cursor: "pointer" }}
            onClick={() => logout()}
          >
            Logout
          </a>
        </>
      );
    } else {
      return <CustomLink to="/dashboard">Signin</CustomLink>;
    }
  }
  return (
    <MainContainer>
      {width > 768 ? (
        <div className="web-menu">
          <CustomLink to="/">Home Page</CustomLink>
          <CustomLink to="/about">About Me</CustomLink>
          <CustomLink to="/Menu2">Menu 2</CustomLink>
          <CustomLink to="/Menu3">Menu 3</CustomLink>
          <CustomLink to="/Menu4">Menu 4</CustomLink>
          {isUser()}
        </div>
      ) : (
        <>
          <div className="menu__list">
            <i className="fa fa-bars" onClick={() => setShow(!show)}></i>
          </div>
          <div className={"mobile-menu" + (show ? " mActive" : " ")}>
            <div className="menu__inner">
              <div className="menu-icon">
                <i className="fa fa-remove" onClick={() => setShow(!show)}></i>
              </div>
              <CustomLink to="/">Home Page</CustomLink>
              <CustomLink to="/about">About Me</CustomLink>
              <CustomLink to="/Menu2">Menu 2</CustomLink>
              <CustomLink to="/Menu3">Menu 3</CustomLink>
              <CustomLink to="/Menu4">Menu 4</CustomLink>
              {isUser()}
            </div>
          </div>
        </>
      )}
    </MainContainer>
  );
});
