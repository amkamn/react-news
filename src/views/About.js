import React, { memo, useEffect } from "react";
import { MainContainer } from "../style/main";
import { MyInfo, MyWrapper, Social } from "../style/about";

export default memo(function About() {
  useEffect(() => {
    document.title = "About Me";
  }, []);
  return (
    <MainContainer>
      <MyWrapper>
        <MyInfo>
          <Social>
            <pre>Contact Me :</pre>
            <a href="https://www.facebook.com/amarjargalmn" target="_blank">
              <i className="fa fa-facebook-square"></i>
            </a>
            <a href="https://gitlab.com/amkamn" target="_blank">
              <i className="fa fa-gitlab"></i>
            </a>
            <a href="https://github.com/amkamn" target="_blank">
              <i className="fa fa-github-square"></i>
            </a>
            <a
              href="https://steamcommunity.com/profiles/76561198095313167/"
              target="_blank"
            >
              <i className="fa fa-steam-square"></i>
            </a>
          </Social>
        </MyInfo>
      </MyWrapper>
    </MainContainer>
  );
});
