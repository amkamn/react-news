import { SET_LOGIN, SET_LOGOUT } from "./types";

const login = () => async dispatch => {
  dispatch({
    type: SET_LOGIN,
    payload: true
  });
};
const logout = () => async dispatch => {
  dispatch({
    type: SET_LOGOUT,
    payload: false
  });
};

export default {
  login,
  logout
};
