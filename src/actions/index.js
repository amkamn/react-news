import ListActions from "./listActions";
import SearchActions from "./searchActions";
import ScrollToTop from "./scroll";
import User from "./user";
const allActions = {
  ListActions,
  SearchActions,
  ScrollToTop,
  User
};

export default allActions;
