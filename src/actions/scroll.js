import { SET_SCROLL } from "./types";

const CalcScroll = () => async dispatch => {
  document.addEventListener("scroll", () => {
    if (window.scrollY > 150) {
      dispatch({
        type: SET_SCROLL,
        payload: true
      });
    } else {
      dispatch({
        type: SET_SCROLL,
        payload: false
      });
    }
  });
};

export default {
  CalcScroll
};
