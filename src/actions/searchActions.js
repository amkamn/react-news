import { FETCH_POSTS, FETCH_ERROR, SET_SEARCH_VALUE } from "./types";
import axios from "axios";

const FetchSearchData = value => async dispatch => {
  try {
    dispatch({
      type: FETCH_ERROR,
      payload: "loading"
    });
    const data = await axios.get(
      `https://newsapi.org/v2/everything?q=${value}&apiKey=${process.env.REACT_APP_API_KEY}`
    );
    if (data.data.status === "ok") {
      if (data.data.articles.length > 0) {
        let passData = {
          loading: "ok",
          items: data.data.articles
        };
        dispatch({
          type: FETCH_POSTS,
          payload: passData
        });
      } else {
        dispatch({
          type: FETCH_ERROR,
          payload: "no-data"
        });
      }
    } else {
      dispatch({
        type: FETCH_ERROR,
        payload: "error"
      });
    }
  } catch (error) {
    dispatch({
      type: FETCH_ERROR,
      payload: "error"
    });
  }
};

const ChangeSearchValue = value => dispatch => {
  dispatch({
    type: SET_SEARCH_VALUE,
    payload: value
  });
};

export default {
  FetchSearchData,
  ChangeSearchValue
};
