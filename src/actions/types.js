export const FETCH_POSTS = "FETCH_POSTS";
export const FETCH_ERROR = "FETCH_ERROR";

export const FETCH_SEARCH_VALUE = "FETCH_SEARCH_VALUE";
export const SET_SEARCH_VALUE = "SET_SEARCH_VALUE";

export const SET_SCROLL = "SET_SCROLL";

export const SET_LOGIN = "SET_LOGIN";
export const SET_LOGOUT = "SET_LOGOUT";
