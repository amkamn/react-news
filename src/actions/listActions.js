import { FETCH_POSTS, FETCH_ERROR } from './types';
import axios from 'axios';

 const FetchData = () => async(dispatch) => {
  try {
      const data = await axios.get(`https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=${process.env.REACT_APP_API_KEY}`);
      if(data.data.status === "ok"){
          if(data.data.articles.length > 0){
            let passData = {
              loading:'ok',
              items:data.data.articles
            };
            dispatch({
              type: FETCH_POSTS,
              payload: passData
            })
          }else{
            dispatch({
              type: FETCH_ERROR,
              payload: 'no-data'
            })
          }
      }else{
        dispatch({
          type: FETCH_ERROR,
          payload: 'error'
        })
      }
  } catch (error) {
    dispatch({
      type: FETCH_ERROR,
      payload: 'error'
    })
  }
};

export default {
  FetchData
}