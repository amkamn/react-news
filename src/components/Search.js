import React from "react";
import { SearchContainer, InputContainer, SearchButton } from "../style/header";
import { useDispatch, useSelector } from "react-redux";
import allActions from "../actions/index";

const Search = () => {
  const value = useSelector(state => state.search.value);
  const dispatch = useDispatch();

  const search = () => {
    if (value.trim().length > 0) {
      dispatch(allActions.SearchActions.FetchSearchData(value));
    }
  };

  const handleKeyPress = event => {
    if (event.key === "Enter") {
      search();
    }
  };
  return (
    <SearchContainer>
      <InputContainer
        placeholder="Search Here ... "
        onChange={e =>
          dispatch(allActions.SearchActions.ChangeSearchValue(e.target.value))
        }
        alue={value}
        onKeyPress={e => handleKeyPress(e)}
      />
      <SearchButton onClick={search}>Search</SearchButton>
    </SearchContainer>
  );
};
export default Search;
