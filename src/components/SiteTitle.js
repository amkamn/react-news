import React,{useState,useEffect} from 'react'
import {Title} from '../style/title';

const SiteTitle = () => {
    const [title,setTitle] = useState('');
    useEffect(() => {
        setTitle(process.env.REACT_APP_TITLE);
    }, [])

    return (
            <Title>{title}</Title>
    )

}
export default SiteTitle;