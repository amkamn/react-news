import React from 'react'
import {CardList,CardListItem,CardImage,CardText} from '../style/content';

const Card = ({data}) => {
        return (
            <CardList>
                <CardListItem>
                    <CardImage src={data.urlToImage} alt={data.title}></CardImage>
                    <CardText href={data.url} target="_blank">{data.title.substr(0,100,)}...</CardText>
                </CardListItem>
            </CardList>
        )
}
export default Card;